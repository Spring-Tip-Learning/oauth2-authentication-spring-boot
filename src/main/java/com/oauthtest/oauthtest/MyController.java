package com.oauthtest.oauthtest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

    @GetMapping("/admin/access")
    ServerTester adminAccess(){
        return new ServerTester(1,"Admin","2019");
    }
    @GetMapping("/user/access")
    ServerTester userAccess(){
        return new ServerTester(1,"user","2019");
    }

    @GetMapping("/publishes")
    ServerTester publishes(){
        return new ServerTester(1,"publishes","2019");
    }


}

class ServerTester{
    private Integer id;
    private String name;
    private String desc;

    public ServerTester() {
    }

    public ServerTester(Integer id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
